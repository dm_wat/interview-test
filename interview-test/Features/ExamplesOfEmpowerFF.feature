﻿Feature: SQT_CS_EN_01_RunSQTMultipleNodes

Background:
	Given User 'AlmostAdmin_1' exists with 'AlmostAdminUserType' User Type
		And The Empower\'Logs' directory is empty
		And Toolkit cleanup then restore has been performed for FAT project 'Valid_OQ1' with 'AlmostAdmin_1' as project owner
		And Toolkit cleanup then restore has been performed for FAT project 'Valid_OQApex' with 'AlmostAdmin_1' as project owner
		And Toolkit cleanup then restore has been performed for FAT project 'Valid_OQMVM' with 'AlmostAdmin_1' as project owner
		And User is logged in Configuration Manager as 'AlmostAdmin_1' having default password
		And Current hostname has been stored as 'host_name'

@Client-Server @LDAP @LocalUser 
@CleanupSqtInfoForCurrentNodeBeforeS @CleanupSqtProjectsBeforeS
Scenario: SQT - Run SQT on multiple nodes, check generated Result Sets and Validation Results and Qualification dates for each Node in English language
	Given Number of entries in the 'Nodes' table of Configuration Manager has been stored as 'number_of_nodes'
	When I send following SQT processing request to all available nodes:
		 | IQ   | OQ   | OQ MVM |
		 | true | true | true   |
		 And I go to 'Nodes' tree node
	Then For each row, these columns are populated with current date in Configuration Manager:
		| Column      |
		| IQ Date     |
		| OQ Date     |
		| OQ MVM Date |
		And SystemsQT log file is created and contains info about successful operations for all available nodes
	When I open the 'Valid_OQ1' project from Configuration Manager
		And I go to 'Result Sets' tab on the Project window
		And I access the View Filter Editor from Project window
		And I add the column 'IQ/OQ Node' to the view filter
		And I save the filter 'show_node' in View Filter Editor
		And I close the View Filter Editor window
	Then The 'Result Sets' tab in Project contains entries having these 'Result Set Name' for each unique 'IQ/OQ Node':
		| Result Set Name         |
		| Internal Std Sample Set |
		| External Std Sample Set |
		| IQ                      |
		And The 'Result Sets' tab in Project contains '3' rows for each unique 'IQ/OQ Node'
	When I close the Project window
		And I close all project applications
		And I open the 'Valid_OQApex' project from Configuration Manager
		And I go to 'Result Sets' tab on the Project window
		And I access the View Filter Editor from Project window
		And I add the column 'IQ/OQ Node' to the view filter
		And I save the filter 'show_node' in View Filter Editor
		And I close the View Filter Editor window
	Then The 'Result Sets' tab in Project contains entries having these 'Result Set Name' for each unique 'IQ/OQ Node':
		| Result Set Name         |
		| Internal Std Sample Set |
		| External Std Sample Set |
		And The 'Result Sets' tab in Project contains '2' rows for each unique 'IQ/OQ Node'
		And The 'Result Sets' tab in Project contains 'number_of_nodes' unique values in 'IQ/OQ Node' column
	When I close the Project window
		And I close all project applications
		And I open the 'Valid_OQMVM' project from Configuration Manager
	Then The 'Validation Results' tab in Project contains '62' rows for each unique 'Study Id'
		And The 'Validation Results' tab in Project contains 'number_of_nodes' unique values in 'Study Id' column